$(function () {
    const na_link = "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/No_image_available_500_x_500.svg/1024px-No_image_available_500_x_500.svg.png"

    let search_query = $('#id_search_query');
    let result_table = $('#result-table');
    let result_count = $('#result-count');
    let page_idx = $('pageidx')
    let page_count = $('pagecount')
    let search_val = '';
    let max_idx = 0;
    let start_idx = 0;
    let max_result = 10;

    $('#search-button').click(function () {
        start_idx = 0;
        search_val = search_query[0].value;
        updateSearch(search_val, max_result, start_idx);
    });

    $('#result-count').change(function () {
        start_idx = 0;
        max_result = parseInt(result_count[0].value);
        updateSearch(search_val, max_result, start_idx);
    });

    $('#prev-button').click(function () {
        if (start_idx >= max_result) {
            start_idx -= max_result;
            updateSearch(search_val, max_result, start_idx);
        }
    });

    $('#next-button').click(function () {
        if (start_idx <= max_idx - max_result) {
            start_idx += max_result;
            updateSearch(search_val, max_result, start_idx);
        }
    });

    function updateSearch(sval, mres, sidx) {
        $.getJSON(`https://www.googleapis.com/books/v1/volumes?q=${sval}&maxResults=${mres}&startIndex=${sidx}`, function (data, stats) {
            max_idx = data.totalItems;
            if (sidx < data.totalItems) {
                page_idx.text(`${(sidx / mres) + 1}`);
                page_count.text(`${Math.floor(max_idx / mres) + 1}`)
                $('.result-row').remove();
                data.items.forEach(e => {
                    let info = e.volumeInfo;
                    let new_row = `<tr class="result-row">
                        <td><div class="table-text">${info.title}</div></td>
                        <td><div class="table-text">${(info.description != undefined) ? info.description : '<b>Empty</b>'}</div></td>
                        <td><div class="table-img"><img src="
                        ${(info.imageLinks != undefined) ? info.imageLinks[Object.keys(info.imageLinks)[0]] : na_link}
                        "></div></td>
                    </tr>`;
    
                    result_table.append(new_row);
                });
            }
        });
    }
});