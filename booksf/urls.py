from django.urls import path

from . import views

app_name = 'booksfinder'

urlpatterns = [
    path('', views.base, name='base'),
]
