from django.shortcuts import render, reverse
from django.http import JsonResponse
from . import forms

# Create your views here.

def base(request):
    context = {
        'search_form' : forms.SearchForm(),
    }

    return render(request, 'booksfinder.html', context)
