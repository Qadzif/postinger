from django.test import TestCase
from .forms import SearchForm

# Create your tests here.

class BooksFinderPageTest(TestCase):
    
    def test_landing_page_url(self):
        response = self.client.get('/booksfinder/')
        self.assertEqual(response.status_code, 200)
    
    def test_use_template(self):
        response = self.client.get('/booksfinder/')
        self.assertTemplateUsed(response, 'booksfinder.html')

class BooksFinderFormTest(TestCase):
    
    def test_form_input(self):
        valid_form = SearchForm({'search_query' : 'oregairu'})
        self.assertTrue(valid_form.is_valid())
