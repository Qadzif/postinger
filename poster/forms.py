from django import forms
from django.utils.translation import gettext_lazy as _
from .models import PostCard

class PostForm(forms.ModelForm):
    class Meta:
        model = PostCard
        exclude = ['status_time']

        labels = {
            'status_text' : ''
        }

        error_messages = {
            'status_text' : {
                'max_length' : _('')
            }
        }
