from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect
from .forms import PostForm
from .models import PostCard

# Create your views here.

def index(request):
    context = {}
    
    context['post_list'] = PostCard.objects.all()

    if request.method == 'GET':
        context['post_form'] = PostForm()
    elif request.method == 'POST':
        post_form = PostForm(request.POST)
        if post_form.is_valid():
            context['post_form'] = PostForm()
            post_form.save()
            return HttpResponseRedirect(reverse('index:index'))
        else:
            context['post_form'] = post_form
            context['error'] = 'Maximum character is 300'

    return render(request, 'index.html', context)
