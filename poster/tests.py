from django.test import TestCase, Client, LiveServerTestCase
from .forms import PostForm
from .models import PostCard
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium import webdriver

# Create your tests here.
_client = Client()

class PosterUrlViewTest(TestCase):

    def test_landing_page_url_get(self):
        response = _client.get('')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_template(self):
        response = _client.get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_landing_page_url_content(self):
        response = _client.get('')
        self.assertIn('Halo, apa kabar?', response.content.decode())

    def test_valid_post(self):
        limit_char = 300 * 'a'
        post_count = len(PostCard.objects.all())
        response = _client.post('', {'status_text' : limit_char})
        post_count = len(PostCard.objects.all()) - post_count
        self.assertEqual(post_count, 1)

    def test_invalid_post(self):
        limit_char = 301 * 'a'
        response = _client.post('', {'status_text' : limit_char})
        self.assertIn('Maximum character is 300', response.content.decode())

class PosterModelTest(TestCase):

    def test_poster_table(self):
        PostCard.objects.create(status_text = 'something something')
        self.assertEqual(len(PostCard.objects.all()), 1)

class PosterFormTest(TestCase):

    def test_valid_poster(self):
        limit_char = 300 * 'a'
        valid_form = PostForm({'status_text' : limit_char})
        self.assertTrue(valid_form.is_valid())

    def test_invalid_poster(self):
        limit_char = 301 * 'a'
        invalid_form = PostForm({'status_text' : limit_char})
        self.assertFalse(invalid_form.is_valid())
    
    def test_form_save(self):
        form_instance = PostForm({'status_text' : 'something something'})
        form_instance.save()
        self.assertEqual(len(PostCard.objects.all()), 1)

    pass

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        opt = Options()
        opt.add_argument('--no-sandbox')
        opt.add_argument('--headless')
        self.selenium = webdriver.Chrome('./chromedriver', options=opt)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        return super().tearDown()

    def test_functional(self):
        selenium = self.selenium

        selenium.get(self.live_server_url)
        status_text = selenium.find_element_by_id('id_status_text')
        submit_button = selenium.find_element_by_id('id_submit_button')

        status_text.send_keys('a' * 300)

        submit_button.send_keys(Keys.RETURN)

        self.assertIn('a' * 300, selenium.page_source)
        pass

    pass
