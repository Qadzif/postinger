from django.db import models
from django.utils import timezone

# Create your models here.

class PostCard(models.Model):
    status_text = models.TextField(max_length=300, default='')
    status_time = models.DateTimeField(auto_now_add=True)