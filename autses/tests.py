from django.test import TestCase
from django.contrib.auth.models import User

# Create your tests here.

class AuthenticationAndSessionTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        user = User.objects.create_user('qadzifkamil', 'randomemail@host.com', 'generalpassword')
        user.first_name = 'Qadzif'
        user.last_name = 'Kamil'
        user.save()

    def test_login_url(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

    def test_loging_in(self):
        response = self.client.post('/login/', {'username' : 'qadzifkamil', 'password' : 'generalpassword'})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/loggedin/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'loggedin.html')

    def test_logout_url(self):
        response = self.client.get('/logout/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'logout.html')
