from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect
from bs4 import BeautifulSoup
import requests, json, random

# Create your views here.

def base(request):
    context = {}

    return render(request, 'accordion.html', context)

def rand_fact(request):
    context = {}

    content = requests.get('http://randomfactgenerator.net/').content.decode()
    content = content[content.find('<html'):]
    content_soup = BeautifulSoup(content, 'html.parser')
    fact = content_soup.findAll('div', {'id' : 'z'})
    fact = fact[random.randint(0, len(fact) - 1)]

    context['content'] = fact.contents[0]

    return render(request, 'base/empty.html', context)
