from django.urls import path

from . import views

app_name = 'accordion'

urlpatterns = [
    path('', views.base, name='base'),
    path('randfact/', views.rand_fact, name='randfact'),
]
