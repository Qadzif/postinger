from django.test import TestCase

# Create your tests here.

class EventlPageTest(TestCase):

    def test_url_get(self):
        response = self.client.get('/accordion/')
        self.assertEqual(response.status_code, 200)

    def test_use_template(self):
        response = self.client.get('/accordion/')
        self.assertTemplateUsed(response, 'accordion.html')

    def test_get_rand_fact(self):
        response = self.client.get('/accordion/randfact/')
        self.assertEqual(response.status_code, 200)
