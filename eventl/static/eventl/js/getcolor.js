var wheel_deg = 0.0;
var target_hsv;
var in_transistion = false;

$(function () {
    var $box2 = $('#box2');
    var $box3 = $('#box3');
    var $box4 = $('#box4');
    var $box5 = $('#box5');
    var $main_col = $('.main-col');
    var switch_button = $('.switch-button');

    const max_dis = $box4.width() / 2;

    // const def_rgb = {r : 255, g : 191, b : 0};
    const def_rgb = {r : 0, g : 11, b : 66};
    const def_hsv = rgb2hsv(def_rgb.r, def_rgb.g, def_rgb.b);
    $box4.css({
        'background' : `linear-gradient(to top, black, transparent), linear-gradient(to right, white, ${quick2css(def_rgb)})`,
        'top' : `${($box2.height() - $box4.height()) / 2}px`
        // 'top' : '50%'
    });
    set_wheel(def_hsv.h, def_hsv.s, def_hsv.v);
    getrgb();

    switch_button.click(function () {
        var hsv_val;
        if (in_transistion) {
            hsv_val = {
                h : target_hsv.h,
                s : target_hsv.s,
                v : 100 - target_hsv.v
            }

            target_hsv = hsv_val;
        } else {
            hsv_val = gethsv();
            target_hsv = hsv_val;
            in_transistion = true;
        }

        $box5.stop().animate({
            top : `${hsv_val.v}%`
        }, 'slow', function () {
            target_hsv = undefined;
            in_transistion = false;

            $main_col.css({
                'background-color' : `${quick2css(getrgb())}`
            });
        });
    });

    $box3.mousedown(function () {
        var changed = $('*');
        changed.addClass('clicked');

        changed.mouseup(function () {
            changed.unbind('mousemove');
            changed.removeClass('clicked');
        });

        // $box2 = $('#box2');

        center = {
            x : $box2.offset().left + $box2.width() / 2,
            y : $box2.offset().top + $box2.height() / 2,
        };

        $('html.clicked').mousemove(function (e) {
            curpos = {x : e.pageX, y : e.pageY};
            dirvec = {
                x : curpos.x - center.x,
                y : curpos.y - center.y
            }

            dir_length = calclength(dirvec)
            _norm_dirvec = {
                x : dirvec.x / dir_length,
                y : dirvec.y / dir_length
            }

            const hue_angle = getangle({
                x : -_norm_dirvec.y,
                y : _norm_dirvec.x
            });
            rgb_val = hsv2rgb(hue_angle, 100, 100);

            rotate_wheel(hue_angle);

            $main_col.css({
                'background-color' : `${quick2css(getrgb())}`
            });
        });
    });

    $box4.mousedown(function () {
        var changed = $('*');
        changed.addClass('clicked');

        changed.mouseup(function () {
            changed.unbind('mousemove');
            changed.removeClass('clicked');
        });

        center = {
            x : $box2.offset().left + $box2.width() / 2,
            y : $box2.offset().top + $box2.height() / 2,
        };

        $('html.clicked').mousemove(function (e) {
            var curpos = {x : e.pageX, y : e.pageY};
            var dirvec = {
                x : curpos.x - center.x,
                y : curpos.y - center.y
            }

            const origin_angle = -45 + wheel_deg;
            const sin_val = Math.sin(deg2rad(origin_angle));
            const cos_val = Math.cos(deg2rad(origin_angle));
            var _rotated_dirvec = {
                x : dirvec.x * cos_val + dirvec.y * sin_val,
                y : dirvec.x * -sin_val + dirvec.y * cos_val
            }

            var _box_dirvec = {
                x : Math.min(max_dis, Math.max(-max_dis, _rotated_dirvec.x)),
                y : Math.min(max_dis, Math.max(-max_dis, _rotated_dirvec.y))
            }

            var _norm_dirvec = {
                x : 1 + _box_dirvec.x / max_dis,
                y : 1 - _box_dirvec.y / max_dis
            }

            set_box(_norm_dirvec.x * 50, _norm_dirvec.y * 50);
            
            $main_col.css({
                'background-color' : `${quick2css(getrgb())}`
            });
        });
    });

    function rotate_wheel(angle) {
        wheel_deg = angle;
        rad_angle = deg2rad(angle)

        _wheel = $box3;
        _box = $box4;

        var x_trans = Math.sin(rad_angle); x_trans = (1 + x_trans) * 50;
        var y_trans = Math.cos(rad_angle); y_trans = (1 - y_trans) * 50;
        _wheel.css({
            'left' : `${x_trans}%`,
            'top' : `${y_trans}%`,
            'transform' : `translate(-${100 - x_trans}%, -${100 - y_trans}%)`
        });

        const top_right = hsv2rgb(wheel_deg, 100, 100);
        _box.css({
            'transform' : `rotate(${-45 + wheel_deg}deg)`,
            'background' : `linear-gradient(to top, black, transparent), linear-gradient(to right, white, ${quick2css(top_right)})`
        });
    }

    function set_wheel(h, s, v) {
        rotate_wheel(h)
        set_box(s, v);

        $('body').css({
            'background-color' : `${quick2css(getrgb())}`
        });
    }

    function set_box(s, v) {
        _box_circle = $box5;
        _box_circle.css({
            'left' : `${s}%`,
            'top' : `${100 - v}%`,
            'transform' : `translate(-50%, -50%)`
        });
    }

    function gethsv() {
        var left_val = $box5.css('left');
        var top_val = $box5.css('top');

        if (left_val.slice(-1) == '%') {
            left_val = parseFloat(left_val.slice(0,-1));
        } else (
            left_val = 50 * parseFloat(left_val.slice(0, -2)) / max_dis
        )

        if (top_val.slice(-1) == '%') {
            top_val = 100 - parseFloat(top_val.slice(0,-1));
        } else (
            top_val = 100 - 50 * parseFloat(top_val) / max_dis
        )

        const s = Math.round(left_val);
        const v = Math.round(top_val);
        const h = wheel_deg;

        return {h : h, s : s, v : v};
    }

    function getrgb() {
        const hsv_val = gethsv();
        return hsv2rgb(hsv_val.h, hsv_val.s, hsv_val.v);
    }
});

function deg2rad(val) {
    return val * Math.PI / 180;
}

function rad2deg(val) {
    return val * 180 / Math.PI;
}

function getangle(vec) {
    vec_length = calclength(vec);
    calc_vec = {
        x : vec.x / vec_length,
        y : vec.y / vec_length
    }

    cos_deg = Math.acos(calc_vec.x); cos_deg = rad2deg(cos_deg);

    if (calc_vec.y >= 0) {
        return cos_deg;
    } else {
        return 360 - cos_deg;
    }
}

function calclength(vec) {
    x = vec.x;
    y = vec.y;

    return Math.pow(x*x + y*y, 0.5);
}

function calcdist(source, target) {
    delvec = {x : source.x - target.x, y : source.y - target.y};

    return calclength(delvec);
}

function quick2css(val) {
    return `rgb(${val.r}, ${val.g}, ${val.b})`;
}

function rgb2hsv(r, g, b) {
    const rgb_val = {r : r, g : g, b : b};
    var rgb_key = ['r', 'g', 'b'];

    var max_key = (r > g) ? 'r' : 'g'; if (b > rgb_val[max_key]) max_key = 'b'; const max_val = rgb_val[max_key];
    var min_key = (r < g) ? 'r' : 'g'; if (b < rgb_val[min_key]) min_key = 'b'; const min_val = rgb_val[min_key];
    rgb_key.splice(rgb_key.indexOf(max_key), 1); rgb_key.splice(rgb_key.indexOf(min_key), 1);
    var mid_key = rgb_key[0]; const mid_val = rgb_val[mid_key];

    const v = Math.round(max_val * 100 / 255);
    const s = Math.round((255 - min_val) * 100 / 255);

    const hue_angle = mid_val * 60 / 255;
    var h = 0;

    switch (max_key + min_key) {
        case 'rb':
            h = hue_angle;
            break;
        case 'gb':
            h = 120 - hue_angle;
            break;
        case 'gr':
            h = 120 + hue_angle;
            break;
        case 'br':
            h = 240 - hue_angle;
            break;
        case 'bg':
            h = 240 + hue_angle;
            break;
        case 'rg':
            h = 360 - hue_angle;
            break;
    }
    h = Math.round(h);


    return {h : h, s : s, v : v};
}

function hsv2rgb(h, s, v) {
    main_val = {};
    s = s / 100;
    v = v / 100;
    d_s = 1 - s;

    h_part = h % 120; h_part = (60 - Math.abs(h_part - 60)) * 255 / 60;
    h_part = Math.floor(h_part);

    switch (Math.floor(h / 60)) {
        case 0:
            main_val = {r : 255, g : h_part, b : 0};
            break;
        case 1:
            main_val = {r : h_part, g : 255, b : 0};
            break;
        case 2:
            main_val = {r : 0, g : 255, b : h_part};
            break;
        case 3:
            main_val = {r : 0, g : h_part, b : 255};
            break;
        case 4:
            main_val = {r : h_part, g : 0, b : 255};
            break;
        case 5:
            main_val = {r : 255, g : 0, b : h_part};
            break;
    }

    var final_val = {}
    for (var key in main_val) {
        final_val[key] = s * main_val[key] + d_s * 255;
        final_val[key] = Math.round(v * final_val[key]);
    }

    return final_val;
}