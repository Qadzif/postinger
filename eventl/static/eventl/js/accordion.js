selected_theme = 0;

$(window).on('beforeunload', function() {
    $(window).scrollTop(0);
});

$(document).ready(function(){

    var accordion_title = $('.accordion-title')
    var color_button = $('#color-button');
    var color_picker = $('#color-picker');

    var switch_button = $('.switch-button');
    var switch_indicator = switch_button.children(0);

    $('#fact-text').load('randfact/');

    chaining(accordion_title, 0, 'slideDown', function () {
        $('#fact-container').animate({opacity : 1});
    });

    switch_button.click(function () {
        need_change = $('.black, .white');

        need_change.toggleClass('white');
        need_change.toggleClass('black');
        $(this).children(0).toggleClass('on');

        switch (selected_theme) {
            case 0:
                switch_button.css({'background-color' : '#2196F3'});
                selected_theme = 1;
                break;
        
            default:
                switch_button.css({'background-color' : 'grey'});
                selected_theme = 0;
                break;
        }
    });

    accordion_title.click(function () {
        _this_content = $(this).siblings();

        _this_content.slideToggle();
    });

    color_button.click(function () {
        color_picker.slideToggle();
    });
});

function chaining(targets, idx, funcname, final) {
    if (idx < targets.length) {
        targets.slice(idx, idx + 1)[funcname]('ease-in', function() {
            chaining(targets, idx+1, funcname, final)
        });
    } else if (isFunction(final)) {
        final();
    }
}

function isFunction(object) {
    return !!(object && object.constructor && object.call && object.apply);
}